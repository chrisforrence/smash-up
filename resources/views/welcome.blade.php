<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="/css/app.css">

        <!-- Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{ env('ANALYTICS_GOOGLE_UA') }}"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag() {dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', '{{ env('ANALYTICS_GOOGLE_UA') }}');
        </script>

    </head>
    <body>
        <div id="app">
            <start-new-game
                :api_url="'{{ getenv('SMASH_UP_API_URL') }}'"></start-new-game>
        </div>
        </section>
        <script type="text/javascript" async src="/js/app.js"></script>
    </body>
</html>
