<?php

namespace App\Http\Middleware;

use Closure;

class ValidateAlexaRequest
{
    protected $timeout = 120;
    
    /**
     * Handle an incoming request.
     * https://developer.amazon.com/docs/custom-skills/host-a-custom-skill-as-a-web-service.html#verifying-that-the-request-was-sent-by-alexa
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Validate SignatureCertChainUrl exists
        if (!$this->isSignatureHeaderPresent($request)) {
            abort(400);
        }
        if (!$this->isSignatureCertChainUrlHeaderPresent($request)) {
            abort(400);
        }
        if (!$this->isSignatureCertChainUrlHeaderFormatted($request)) {
            abort(400);
        }
        $pem = $this->getPEM($request);

        if ($pem === null) {
            abort(400);
        }
        $certificate = $this->getCertificate($pem);

        if ($certificate === null) {
            abort(400);
        }
        if (!$this->hasCertificateNotExpired($certificate)) {
            abort(400);
        }
        if (!$this->doesCertificateHaveValidSANS($certificate)) {
            abort(400);
        }
        if (!$this->isSignatureChainValid($request, $pem)) {
            abort(400);
        }
        if (!$this->doesApplicationIDMatch($request)) {
            abort(400);
        }
        if (!$this->hasRequestNotTimedOut($request)) {
            abort(400);
        }
        if (!$this->doHashesMatch($request, $pem)) {
            abort(400);
        }
        return $next($request);
    }

    private function doesApplicationIDMatch($request) {
        return $request->input('session.application.applicationId') === getenv('AWS_ALEXA_ID');
    }

    private function hasRequestNotTimedOut($request) {
        return strtotime($request->input('request.timestamp')) + $this->timeout > time();
    }

    private function isSignatureHeaderPresent($request) {
        return $request->header('Signature') !== null;
    }

    private function doHashesMatch($request, $pem) {
        if (!($publicKey = openssl_pkey_get_public($pem))) {
            return false;
        }
        openssl_public_decrypt($request->header('Signature'), $decryptedSignature, $publicKey);

        $hash = sha1($request->getContent());

        return substr($decryptedSignature, 30) === $hash;
    }

    private function doesCertificateHaveValidSANS($certificate) {
        return stripos($certificate['extensions']['subjectAltName'], 'echo-api.amazon.com') !== false;
    }

    private function isSignatureCertChainUrlHeaderPresent($request) {
        return $request->header('SignatureCertChainUrl') !== null;
    }

    private function isSignatureChainValid($request, $pem) {
        try {
            return openssl_verify($request->getContent(), $request->header('Signature'), $pem, 'sha1') === 1;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function getPEM($request) {
        $pemHashFile = sys_get_temp_dir() . '/' . hash('sha256', $request->header('SignatureCertChainUrl')) . '.pem';
        if (!file_exists($pemHashFile)) {
            file_put_contents($pemHashFile, file_get_contents($request->header('SignatureCertChainUrl')));
        }
        return file_get_contents($pemHashFile);
    }

    private function getCertificate($pem)
    {
        $certificate = openssl_x509_parse($pem);
        return empty($certificate) ? null : $certificate;
    }

    private function isSignatureCertChainUrlHeaderFormatted($request) {
        $parts = parse_url($request->header('SignatureCertChainUrl'));
        return strtolower($parts['scheme']) === 'https'
            && strtolower($parts['host']) === 's3.amazonaws.com'
            && (!array_key_exists($parts, 'port') || $parts['port'] === 443)
            && strpos($parts['path'], '/echo.api/') === 0;
    }
    private function hasCertificateNotExpired($certificate) {
        $now = time();
        return $certificate['validFrom_time_t'] < $now && $now < $certificate['validTo_time_t'];
    }
}
